# react-whiteboard

## TODO

For this project we are using the [swapi public API](https://swapi.co/).

- [ ] Display a loader for ever API calls
- [ ] Render the list of StarWars characters
- [ ] Clicking on one of the charachter should display their details on a right handside panel. Details to includes:
  - [ ] The list of movies they were acting (name and year of production)
  - [ ] Details about their planet of Origin (name, diameter and population)

## What's happening

One of our junior dev started to build somehing using CRA. However he's made few mistakes that needs to be fixed in order to continue.
He didn't managed to render the list of characters and he's not really sure why. Help him to identify his error(s), give him tips on the best practices, explain him why and finish the task from the todo.

You have 30-40 minutes to do so